'use strict';

const express       = require('express');
const bodyParser    = require('body-parser');
const models        = require('./models');
const moment        = require('moment');
const elasticsearch = require('elasticsearch');
const api           = express();

const CONTENT_TYPE = 'application/json';
const ADMIN_TOKEN  = "idontcodeandtell";

const authenticateAdmin = (req) => {
	return req.headers['admin_token'] === ADMIN_TOKEN;
};

const validateDatatype = (req) => {
	return req.get('Content-Type') === CONTENT_TYPE;
}

const client = new elasticsearch.Client({
	host : 'localhost:9200',
	log  : 'error'
});

/* Returns details about a company with the given ID. If the company is
   not found, 404 is returned. It returns data from MongoDB, not ElasticSearch. */
api.get('/companies/:id', (req, res) =>  {
	models.Companies.findOne({'_id' : req.params.id}, function(err, doc) {
		if (err) {
			res.status(500).send("Something went wrong");
			return;
		}
		if (doc === null) {
			res.status(404).send("Company not found");
			return;
		}

		var company = {
			id          : doc._id,
			title       : doc.title,
			description : doc.description,
			url         : doc.url
		};

		res.json(company);
	});
});

api.post('/companies/search', bodyParser.json(), (req, res) => {
	console.log(req.body);
	client.search({
		index : 'companies',
		type : 'company',
		body : {
			query : {
				bool : {
					'should' : [
						{ 'term' : {'title' : req.body.search} },
						{ 'term' : {'description' : req.body.search}}
					]
				}
			}
		}
	}, (err, doc) => {
		if (err) {
			console.log(err.message);
			res.status(500).send(err.message);
			return;
		}

		const results = [];
		doc.hits.hits.forEach((elem) => {
			results.push({
				'id':          elem._source._id,
				'title':       elem._source.title,
				'description': elem._source.description,
				'url':         elem._source.url
			})
		});
		res.send(results);
	});
});

/* Updates a company in MongoDB and re-indexes the document, this endpoint is authorized with
   token named ADMIN_TOKEN.
   If company with id is not found, 404 is returned. If the model is invalid, 412
   is returned. If the content sent by client contains no changes, i.e. the company data
   unchanged, then 400 will be returned - along with the message "Company not modified." */
api.post('/companies/:id', bodyParser.json(), (req, res) => {
	// Check if the admin token is correct
	// Returns error code 401 if the token is not correct
	if(!authenticateAdmin(req)) {
		res.status(401).send("You don't have permission to create a company");
		return;
	}

	// Check if content-type is application/json
	if (!validateDatatype(req)) {
		res.status(415).send("Content-type must me application/json");
		return;
	}

	const c = new models.Companies(req.body);
	c.validate((err) => {
		if (err) {
			res.status(412).send('Some required properties are missing.');
			return;
		}

		const updatedData = {
			title       : req.body.title,
			description : req.body.description,
			url         : req.body.url
		};

		// Update the company
		models.Companies.update({'_id' : req.params.id}, updatedData, function(err, updateDoc) {
			if (err) {
				res.status(500).send("Something went wrong");
				return;
			}
			if (!updateDoc.n) {
				res.status(404).send("Company not found");
				return;
			}
			if(!updateDoc.ok) {
				res.status(404).send("Company not found");
				return;
			}
			if(!updateDoc.nModified) {
				res.status(400).send("Company was not modified");
				return;
			}

			// Update elasticsearch
			client.update({
				index : 'companies',
				type  : 'company',
				id    : String(req.params.id),
				body  : {
					doc : {
						title       : updatedData.title,
						description : updatedData.description,
						url         : updatedData.url
					}
				}
			}).then((doc) => {
				res.status(201).send({
					'id' : doc._id
				});
			}, (err) => {
				console.log(err);
				res.status(500).send(err.message);
			});
		});
		// });
	});
});

/* Adds new company to Punchy, saves it to MongoDB and indexes it in ElasticSearch.
   This endpoint is authorized with token named ADMIN_TOKEN.
   If the model is invalid, 412 is returned */
api.post('/companies', bodyParser.json(), (req, res) => {
	// Check if the admin token is correct
	// Returns error code 401 if the token is not correct
	if(!authenticateAdmin(req)) {
		res.status(401).send("You don't have permission to create a company");
		return;
	}

	// Check if content-type is application/json
	if (!validateDatatype(req)) {
		res.status(415).send("Content-type must me application/json");
		return;
	}

	const c = new models.Companies(req.body);

	c.validate((err) => {
		if (err) {
			res.status(412).send('Some required properties are missing.');
			return;
		}

		// Check if company exists with the same name. status code 409 if it does.
		models.Companies.find({'title' : req.body.title}, function(err, doc) {
			if (err) {
				res.status(500).send(err);
				return;
			}
			if (doc.length > 0) {
				res.status(409).send("There is already a company with that title");
				return;
			}

			c.save((err, doc) => {
				if (err) {
					res.status(500).send(err.message);
					return;
				}

				// Add company to elastic search
				client.index({
					index: 'companies',
					type: 'company',
					id: String(doc._id),
					body: doc
				}).then((doc) => {
					res.status(201).send({
						'id' : doc._id,
					});
				}, (err) => {
					res.status(500).send(err.message);
				});
			});
		});
	});
});

/* Returns a list of all companies from ElasticSearch. Pagination is controlled by the
   client via the query parameters page and max. They default to 0 and 20, respectively.
   Companies are sorted alphabetically by their titles. If no companies are found, an empty
   array is returned. */
api.get('/companies', (req, res) => {
	const page = req.query.page || 0;
	const max  = req.query.max || 20;

	// query elastic search for all companies, sorted by title.raw
	// and paged according to query params page and max.
	client.search({
		index: 'companies',
		type:  'company',
		body: {
			sort: ['title.raw'],
			from: page * max,
			size: max
		},
	}, (err, doc) => {
		if (err) {
			console.log(err);
			res.status(500).send(err.message);
			return;
		}
		const results = [];

		doc.hits.hits.forEach((elem) => {
			results.push({
				'id':          elem._source._id,
				'title':       elem._source.title,
				'description': elem._source.description,
				'url':         elem._source.url
			})
		});

		res.send(results);
	});
});

/* Removes a company from MongoDB and from ElasticSearch. If company is not found
   404 is returned. Upon successful removal of the company, 204 is returned. */
api.delete('/companies/:id', (req, res) => {
	models.Companies.findByIdAndRemove(req.params.id, (err, doc) => {
		if (err) {
			res.status(500).send(err.message);
			return;
		}
		if (!doc) {
			res.status(404).send('Company not found.');
			return;
		}

		client.delete({
			index: 'companies',
			type:  'company',
			id:    req.params.id
		}, (err, doc) => {
			if (err) {
				res.status(500).send(err.message);
				return;
			}

			res.status(204).send();
		})
	});
});


module.exports = api;

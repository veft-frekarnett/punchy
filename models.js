'use strict';

const mongoose = require('mongoose');
var uuid       = require('node-uuid');
var moment     = require('moment');

const CompaniesSchema = mongoose.Schema({
	// IDs are auto generated
	title       : { type : String, required : true }, 	// String representing the company name
	description : { type : String, required : true }, 	// String representing description for the company
	url         : { type : String, required : true}, 	// String representing the company homepage
	created     : { type : Date, required : true, default : moment()} 	// Date representing the creation date of the company
});

module.exports = {
	Companies  : mongoose.model('Companies', CompaniesSchema)
};
